## Descubriendo Puertos

nmap -sV  192.168.96.137 -T4

Starting Nmap 7.60 ( https://nmap.org ) at 2019-11-19 23:07 -05.

Stats: 0:00:11 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan.

Connect Scan Timing: About 22.00% done; ETC: 23:08 (0:00:43 remaining)

Nmap scan report for 192.168.96.137

Host is up (0.76s latency).

Not shown: 998 filtered ports

>PORT   STATE  SERVICE VERSION
>
>22/tcp closed ssh
>
>80/tcp open   http    Apache httpd 2.2.15 ((Fedora))

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .

Nmap done: 1 IP address (1 host up) scanned in 42.35 seconds

Paragraph.



## Analisando las vulnerabilidades

- List item 1
- List item 2

## Con Nmap --script vuln 
nmap -p 80 --script vuln  192.168.96.137 -T4

Starting Nmap 7.60 ( https://nmap.org ) at 2019-11-19 23:09 -05

Pre-scan script results:

| broadcast-avahi-dos: 

|   Discovered hosts:

|     224.0.0.251

|   After NULL UDP avahi packet DoS (CVE-2011-1002).

|_  Hosts are all up (not vulnerable).
Stats: 0:01:05 elapsed; 0 hosts completed (1 up), 1 undergoing Script Scan
NSE Timing: About 96.33% done; ETC: 23:10 (0:00:01 remaining)
Stats: 0:01:05 elapsed; 0 hosts completed (1 up), 1 undergoing Script Scan
NSE Timing: About 96.33% done; ETC: 23:10 (0:00:01 remaining)
Nmap scan report for 192.168.96.137
Host is up (0.00047s latency).

PORT   STATE SERVICE
80/tcp open  http
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
| http-enum: 
|_  /icons/: Potentially interesting folder w/ directory listing
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-trace: TRACE is enabled
| http-vuln-cve2011-3192: 
|   VULNERABLE:
|   Apache byterange filter DoS
|     State: VULNERABLE
|     IDs:  CVE:CVE-2011-3192  OSVDB:74721
|       The Apache web server is vulnerable to a denial of service attack when numerous
|       overlapping byte ranges are requested.
|     Disclosure date: 2011-08-19
|     References:
|       http://osvdb.org/74721
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-3192
|       http://seclists.org/fulldisclosure/2011/Aug/175
|       http://nessus.org/plugins/index.php?view=single&id=55976
|_      http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-3192

Nmap done: 1 IP address (1 host up) scanned in 171.81 seconds

## Con Nmap -A
nmap -p 80 -A  192.168.96.137 -T4

Starting Nmap 7.60 ( https://nmap.org ) at 2019-11-19 23:13 -05
Nmap scan report for 192.168.96.137
Host is up (0.0011s latency).

PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.2.15 ((Fedora))
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.2.15 (Fedora)
|_http-title: Hackademic.RTB1  

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 7.03 seconds

## Explorando la pagina web

### Manualmente

![Pagina de inicio ](img/PaginaInicial.png) 
http://192.168.96.137/Hackademic_RTB1/?cat=5
![Pagina de Busqueda ](img/PaginaDeBusqueda.png) 
http://192.168.96.137/Hackademic_RTB1/?cat=%22%22
![Error de Pagina ](img/ErrorDePagina.png) 

### Dirb

![Enumeracion Dirb](img/EnumeracionDirb.png) 

### Explotando SQLI con nmap
sqlmap -u http://192.168.96.137/Hackademic_RTB1/?cat=5 
![Excel de SQL](img/SpreadSheetSQL.png) 